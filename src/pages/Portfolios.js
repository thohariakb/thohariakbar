import React from 'react';
import Footer from '../components/Footer';
import '../App.css';
import Navbar from "../components/Navbar";
import { Row, Col } from 'react-bootstrap'
import techStopImg from '../images/techstop.png'
import meowMeImg from '../images/meowmetv.png';

function Portfolios() {
  return (
    <>
<Navbar />    
<div className="container justify-content-center mt-5">

<div className="item content-containers mt-5"> 

<section id="portfolios">
    <div className="container justify-content-center">
      <div className="row ms-5 mt-5 mb-3 d-flex justify-content-center">
        <div className="col">
          <h1>Portfolios</h1>
          <p>Here are my other portfolios.</p>
        </div>
      </div>
    
      <div className="row ms-5 mt-5 mb-3 d-flex justify-content-center cards">
  
        {/* <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/Clima-App" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">Clima-App</h5>
              <p className="card-text">Clima App - A awesome weather android app that shows you the temperature accurately on your current location and you can choose location manually also.</p>
            </div>
          </div>
        </div>
  
        
        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/AyoKerja" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">AyoKerja</h5>
              <p className="card-text">Ayo Kerja is a talent agency platform for helping recruiters with various professional talents.</p>
            </div>
          </div>
        </div>
  
        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/FlashChat-Firebase-App" target="_blank"  alt="Service One"></a>
              <h5 className="card-title">FlashChat Firebase Android App</h5>
              <p className="card-text">An android messenger app that allows you to make a private chat with your friends.</p>
            </div>
          </div>
        </div> */}

        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <a href="http://tech-stop.herokuapp.com/" target="_blank">
            <img src={techStopImg} className="card-img-top w-100" alt="techstop-image"/>
            </a>
            <div className="card-body">
              <a className="icon fa fa-gitlab fa-2x" href="https://gitlab.com/binarxglints_batch11/finalproject/team-d/front-end" target="_blank"  alt="Techstop"></a>
              <h5 className="card-title">Tech Stop</h5>
                <p className="card-text text-start">Tech Stop is a marketplace based on web application that is providing services for your damaged stuff, it is an one-stop marketplace for all your technical service needs!
                </p>
                <p className="card-text text-start">
                  Customers can conveniently hire the best service for the best price,
                  customers can conveniently hire the best service for the best price.
                </p>
            </div>
          </div>
        </div>


        <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <a href="http://meowmetv.herokuapp.com/" target="_blank">
              <img src={meowMeImg} className="card-img-top w-100" alt="meowme-image"/>
            </a>
            <div className="card-body">
              <a className="icon fa fa-gitlab fa-2x" href="https://gitlab.com/thohariakb/meowmetv/" target="_blank"  alt="Techstop"></a>
              <h5 className="card-title">Meowme</h5>
                <p className="card-text text-start">
                Meowme offers movie information that has released and the upcoming movie releases, If you are looking for what ordinary viewing folk think of this week’s releases, there is really nowhere else than Meowme. 
                </p>
                <p className="card-text text-start">
                There are countless user reviews of each film title, information of cast actors, ranging from a 1-star review up to a 5!
                </p>
            </div>
          </div>
        </div>

        {/* <div className="col-md-4 d-md-flex justify-content-center">
          <div className="card" style={{width: '22rem', marginTop: '20px'}}>
            <div className="card-body">
              <a className="icon fa fa-github-alt fa-2x" href="https://github.com/thohariakb/AyoKerja" target="_blank" rel="noreferrer" alt="Service One"></a>
              <h5 className="card-title">AyoKerja</h5>
              <p className="card-text">Ayo Kerja is a talent agency platform for helping recruiters with various professional talents.</p>
            </div>
          </div>
        </div>
   */}
        
  
      </div>

    </div>
    </section>
  </div>
 </div>
 <Footer />
    </>
  );
}

export default Portfolios;
