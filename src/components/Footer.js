import React from 'react';

function Footer() {
  return (
    <>
    <section id="footer">
    <div className="container d-md-flex justify-content-center">
      <div className="row align-items-center">
          <p className="copyright">Copyright &copy; 2021 | Thohari Akbar </p>
      </div>
    </div>
   </section>
    </>
  );
}

export default Footer;
